class Customer {
  constructor(email) {
    this.email = email;
    this.cart = new Cart();
    this.orders = [];
  }
  checkOut() {
    if (this.cart) {
      this.cart.computeTotal();
      this.orders.push(this.cart);
    }
    return this;
  }
}

class Product {
  constructor(name, price) {
    this.name = name;
    this.price = price;
    this.isActive = true;
  }
  updatePrice(price) {
    this.price = price;
    return this;
  }
  archive() {
    this.isActive = false;
    return this;
  }
}

class Cart {
  constructor() {
    this.contents = [];
    this.totalAmount = 0;
  }
  addToCart(product, quantity) {
    this.contents.push({ product, quantity: quantity });
    return this;
  }
  showCartContents() {
    console.log(this.contents);
    return this;
  }
  updateProductQuantity(product, quantity) {
    this.contents.forEach((element) => {
      if (element.product.name === product) {
        element.quantity = quantity;
      }
    });
    return this;
  }
  clearCartContents() {
    this.contents = [];
    return this;
  }
  computeTotal() {
    let totalAmount = 0;
    this.contents.forEach((element) => {
      totalAmount = totalAmount + element.product.price * element.quantity;
    });
    this.totalAmount = totalAmount;
    return this;
  }
}
const john = new Customer("john@mail.com");

const prodA = new Product("soap", 9.99);
const prodB = new Product("shampoo", 12.99);
const prodC = new Product("toothbrush", 4.99);
const prodD = new Product("toothpaste", 14.99);

john.cart.addToCart(prodA, 3);
john.cart.addToCart(prodB, 2);

//john.cart.computeTotal()
john.cart.updateProductQuantity("soap", 5);
john.cart.showCartContents();
//john.cart.clearCartContents()
//john.cart.showCartContents()
john.checkOut();
